# README

本项目后端为微服务架构，因此设置git子模块形式对各微服务进行存储和管理。

具体使用方法参见： https://www.jianshu.com/p/e27a978ddb88 

## ROUTES

服务器1：121.199.79.177

服务器2： 47.103.196.193

- Eureka地址：121.199.79.177:11000

  - 用户：eureka
  - 密码：password

  > 可以登录eureka查看服务运行情况

- Zuul地址：121.199.79.177:11001


## APIS

后端接入地址：http://121.199.79.177:11001

API文档地址：http://121.199.79.177:11001/swagger-ui.html

### 用户服务部分

```yml
# api前缀
PREFIX: /user/api
# 所有api说明
DETAILED-APIS: /user/api/swagger-ui.html
```

### 图板服务部分

```yml
# api前缀
PREFIX: /board/api
# 所有api说明
DETAILED-APIS: /board/api/swagger-ui.html
```

### 消息服务部分

```yml
# api前缀
PREFIX: /message/api
# 使用web socket协议
WS-URL: /message/api/{{userId}}
WS-MESSAGE-MODEL: 
```

### 计算服务部分

```yml
# api前缀
PREFIX: /compute/api
# 所有api说明
DETAILED-APIS: /compute/api/swagger-ui.html
```